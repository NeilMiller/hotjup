function pl_profiles = get_profiles_inflated
    %% This function is designed specifically to determine the entropy for
    %%  HD 209458, TrES-4, and WASP-12
    %% then it should construct a hydrostatic equilibrium structure for
    %% each of these and return a vector of all three
    
    load solar_system_constants.mat
    
    %% HD 209458    
    T = 5942 ;
    Rs = 1.146 * SOLARRADIUS;
    Ls = 4 * pi * SIGMA * T^4 * Rs^2;
    a = 0.04707 * AU;
    Mp = 0.685 * JUPITERMASS;
    Rp = 1.32 * JUPITERRADIUS;
    incidentFlux = Ls / (4 * pi * a^2);
    
    
    
    
end