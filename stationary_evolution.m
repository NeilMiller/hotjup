function [T,Y,Rp_vec] = stationary_evolution(MpJ, aAU, Ls)

    global grid %% declare the planet grid to be a global variable.  
                 %% i don't want this to be loaded multiple times
    global AU EARTHMASS G JUPITERMASS JUPITERRADIUS SIGMA SOLARMASS SOLARRADIUS YR sconv
    
    global entropy_coeffs mass_coeffs flux_coeffs
    
    load solar_system_constants.mat
    load grid40.mat
    
    grid = grid40;
    entropy_coeffs = grid_coeffs(grid.entropyVect);
    mass_coeffs = grid_coeffs(grid.MassVect);
    flux_coeffs = grid_coeffs(grid.fluxVect);
    
    %Ms = 0.93 * SOLARMASS;
    Mp = MpJ*JUPITERMASS ; %% planet mass
    a = aAU * AU ; %% semi-major axis
    %Tstar = 5274;
    %Rs = 0.87 * SOLARRADIUS;
    %Ls = 4 * pi * Rs^2 *SIGMA * Tstar^4;
    params = [Ls a Mp];
    
    tspan = [0, 1e10 * YR];
    
   % entropy = min([(log10(Mp / JUPITERMASS) + 4.32) / 0.39 , max(grid.entropyVect)-1]) ; %% planet entropy
    entropy = grid.entropyVect(1);
    y0 = [entropy];
    
    options = odeset('MaxStep', 1e6 * YR, 'RelTol', 1e-4);
    [T,Y] = ode45(@(t,y)stationary_ev(t,y,params), tspan, y0, options);
    
    flux = Ls / (4 * pi * a^2);
    
    Rp_vec = zeros(length(Y),1);
    for i=1:length(Y)
        [Teff, Rp, Ipl, Lpl, dEds] = get_planet_properties(Mp, Y(i), flux);
        Rp_vec(i) = Rp;
    end
end