function output = load_adiabats(inputfile)
    [density, pressure, dpdum, entropy, temperature X Z] = textread(inputfile, '%f %f %f %f %f %f %f');
    
    profz = find(entropy == entropy(1));
    arraysize = size(profz, 1);
    numprofiles = size(density,1) / arraysize;  %% This figures out the dimensions of the file
    
    output = struct('entropy_vect', zeros(numprofiles,1), 'pressure_mat', zeros(numprofiles,arraysize), ...
        'temp_mat', zeros(numprofiles,arraysize), 'density_mat', zeros(numprofiles, arraysize), 'arraysize', arraysize,...
        'numprofiles', numprofiles);
    
    for i=1:numprofiles

        output.entropy_vect(i) = entropy((i-1)*arraysize+1);
        ind = (i-1)*arraysize+1:i*arraysize ;
        output.pressure_mat(i,1:arraysize) = pressure(ind);
        output.temp_mat(i,1:arraysize) = temperature(ind);
        output.density_mat(i,1:arraysize) = density(ind);
    end
    
end