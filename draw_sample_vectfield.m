function draw_sample_vectfield
    global AU EARTHMASS G JUPITERMASS JUPITERRADIUS SIGMA SOLARMASS SOLARRADIUS YR sconv
    global grid
    global entropy_coeffs mass_coeffs flux_coeffs
    load solar_system_constants.mat
    load planet_grid_10ME.mat
    
    entropy_coeffs = grid_coeffs(grid.entropyVect);
    mass_coeffs = grid_coeffs(grid.MassVect);
    flux_coeffs = grid_coeffs(grid.fluxVect);
    
    
    t = 1d9 * YR;
    Ms = SOLARMASS;
    Ls = 3.826e33;
    Rs = SOLARRADIUS;
    Qp = 1e7;
    Qs = 1e6;
    e = 0.;
    avect = 0.01:0.001:0.05;
    Mpvect = 10.^(-0.9:0.1:0.9);
    entropy = 7;

    Omegas = 0.000 ; %% Stellar spin rate

    size(avect)
    size(Mpvect)
    params = [Ms Ls Rs Qp Qs];
    [semi_mat, Mp_mat] = meshgrid(avect, Mpvect);

    
    N = length(avect);
    M = length(Mpvect);
    dy = zeros(N,M,6);
    da = zeros(N,M);
    dMp = zeros(N,M);
    Tau = zeros(M,N);
    for i=1:N
        for j=1:M
            a = avect(i) * AU;
            mu = G * (Ms);
            Period = 2 * pi * sqrt(a^3/mu);
            n = 2 * pi / Period;
            Omegap = n ; %% planet spin rate
            Mp = Mpvect(j) * JUPITERMASS;
            y = [a e Mp entropy Omegap Omegas];    
            dy(i,j,:) = tidal_ev2(t, y, params);
            da(i,j) = dy(i,j,1) / AU;
            dMp(i,j) = dy(i,j,3) / JUPITERMASS;
            Tau(j,i) = (dy(i,j,1)/a + dy(i,j,3)/Mp)^(-1);
        end
    end
    da
    dMp
%    quiver(semi_mat, Mp_mat, da, dMp);
    contour(semi_mat, Mp_mat, log10(Tau/YR));

end