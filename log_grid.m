function grid = log_grid(minValue, maxValue, Number)
    A = log10(minValue);
    B = log10(maxValue);
    Sp = (B-A) / (Number - 1);
    grid = 10.^(A:Sp:B);
end