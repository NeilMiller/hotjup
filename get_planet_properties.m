function [Teff, Rp, Ipl, Lpl, dEds] = get_planet_properties(Mp, entropy, flux)
    global grid
    global entropy_coeffs mass_coeffs flux_coeffs
    global AU EARTHMASS G JUPITERMASS JUPITERRADIUS SIGMA SOLARMASS SOLARRADIUS YR sconv
    %%% Figure out Rp, Ipl, Lpl, dEds, Teff from entropy, mass, semi-major axis
    %%% This should work via a hash function.  It is important that not too
    %%% much time is spent on this since it has to happen a lot.
    
    %% There is a slight issue that I have decided doesn't matter which is
    %% that the entropy grid is not exactly log spaced, however the error
    %% introduced by this is small
    
    entropy_estind = (log10(entropy) - entropy_coeffs(2)) / entropy_coeffs(1);
    if(entropy_estind < 1)
        entropy_estind = 1.001;
    elseif(entropy_estind > length(grid.entropyVect))
        entropy_estind = length(grid.entropyVect)-0.001;
    end
    mass_estind = (log10(Mp/JUPITERMASS) - mass_coeffs(2)) / mass_coeffs(1);
    if(mass_estind < 1)
        mass_estind = 1.001;
    elseif(mass_estind > length(grid.MassVect))
        mass_estind = length(grid.MassVect)-0.001;
    end
    flux_estind = (log10(flux) - flux_coeffs(2)) / flux_coeffs(1);
    if(flux_estind < 1)
        flux_estind = 1.001;
    elseif(flux_estind > length(grid.fluxVect))
        flux_estind = length(grid.fluxVect)-0.001;
    end
    
    entropy_lowind = floor(entropy_estind);
    entropy_highind = entropy_lowind + 1;
    entropy_alpha = (entropy_estind - entropy_lowind);    
    
    mass_lowind = floor(mass_estind);
    mass_highind = mass_lowind + 1;
    mass_alpha = (mass_estind - mass_lowind);
    
    flux_lowind = floor(flux_estind);
    flux_highind = flux_lowind + 1;
    flux_alpha = (flux_estind - flux_lowind);
    
    wts3 = zeros(2,2,2);
    wts2 = zeros(2,2);
    
    %% i didn't want to figure out how to do this correctly with
    %% multiplication so i just did this
    wts3(1,1,1) = (1-mass_alpha)*(1-entropy_alpha) * (1-flux_alpha);
    wts3(1,1,2) = (1-mass_alpha)*(1-entropy_alpha) * (flux_alpha);
    wts3(1,2,1) = (1-mass_alpha)*(entropy_alpha) * (1-flux_alpha);
    wts3(1,2,2) = (1-mass_alpha)*(entropy_alpha) * (flux_alpha);
    wts3(2,1,1) = (mass_alpha)*(1-entropy_alpha) * (1-flux_alpha);
    wts3(2,1,2) = (mass_alpha)*(1-entropy_alpha) * (flux_alpha);
    wts3(2,2,2) = (mass_alpha)*(entropy_alpha) * (flux_alpha);
    
    wts2(1,1) = (1-mass_alpha)*(1-entropy_alpha);
    wts2(1,2) = (1-mass_alpha)*(entropy_alpha);
    wts2(2,1) = (mass_alpha)*(1-entropy_alpha);
    wts2(2,2) = (mass_alpha)*(entropy_alpha);
    
    function val = dot3(A,B)
        val = A(1,1,1)*B(1,1,1) ...
            + A(1,1,2)*B(1,1,2) ...
            + A(1,2,1)*B(1,2,1) ...
            + A(1,2,2)*B(1,2,2) ...
            + A(2,1,1)*B(2,1,1) ...
            + A(2,1,2)*B(2,1,2) ...
            + A(2,2,2)*B(2,2,2) ;
    end
    
    function val = dot2(A,B)
        val = A(1,1)*B(1,1) ...
            + A(1,2)*B(1,2) ...
            + A(2,1)*B(2,1) ...
            + A(2,2)*B(2,2) ;
    end

    Teff = dot3(grid.Teff(mass_lowind:mass_highind, entropy_lowind:entropy_highind, flux_lowind:flux_highind), wts3);
    Rp = dot2(grid.PlanetRadius(mass_lowind:mass_highind, entropy_lowind:entropy_highind), wts2);
    Ipl = dot2(grid.MomentOfInertia(mass_lowind:mass_highind, entropy_lowind:entropy_highind), wts2);
    Lpl = dot3(grid.LumInt(mass_lowind:mass_highind, entropy_lowind:entropy_highind, flux_lowind:flux_highind), wts3);
    dEds = dot2(grid.dEds(mass_lowind:mass_highind, entropy_lowind:entropy_highind), wts2);

end