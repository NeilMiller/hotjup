function dy = tidal_ev2(t, y, params)
    function U1 = potenL1(Mp, Ms, a, z)
        r1 = (a - z) / a;
        r2 = z / a;
        U1 = (1 - Mp / Ms) * (1 / r1 + r1^2 / 2) + (Mp / Ms) * (1 / r2 + r2^2 / 2);
    end

    function U2 = potenL2(Mp, Ms, a, z)
        r1 = (a + z) / a;
        r2 = z / a;
        U2 = (1 - Mp / Ms) * (1 / r1 + r1^2 / 2) + (Mp / Ms) * (1 / r2 + r2^2 / 2);
    end

    %% These commands should be run before calling this function such that
    %% these variables are already defined
    global grid %% declare the planet grid to be a global variable.  
                 %% i don't want this to be loaded multiple times
    global AU EARTHMASS G JUPITERMASS JUPITERRADIUS SIGMA SOLARMASS SOLARRADIUS YR sconv
    %%% these constants should be delcared global and loaded with 
    %%    load solar_system_constants.mat
    %%% In the controlling file
    k = 1.380658e-16;
    mH = 1.67e-24;
    
    
    t/YR
    
    dy = zeros(6,1); %%  a , e, Mp, s, Omegap, Omegas
    %% values are generally in cgs and integrated in cgs.
    a = y(1) ; %% semi-major axis
    e = y(2) ; %% eccentricity
    Mp = y(3) ; %% planet mass
    entropy = y(4) ; %% planet entropy
    Omegap = y(5) ; %% planet spin rate
    Omegas = y(6) ; %% Stellar spin rate
    
    Ms = params(1) ;
    Ls = params(2) ;
    Rs = params(3) ;
    Qp = params(4) ;
    Qs = params(5) ;
    
    Lxuv = 1e-3 * Ls;
    
    flux = Ls / (4 * pi * a^2);
    
    if(entropy > ((log10(Mp / JUPITERMASS) + 4.32) / 0.39)) %% this was an empirically determined relation where 
        warning('Planets mass/entropy is in the forbiden zone.'); %#ok<WNTAG>
        disp(entropy);
        disp(Mp/JUPITERMASS);
        disp(t/YR);
    end

    %%% Figure out Rp, Ipl, Lpl, dEds, Teff from entropy, mass, semi-major axis
    %%% This should work via a hash function.  It is important that not too
    %%% much time is spent on this since it has to happen a lot.
    
    %% There is a slight issue that I have decided doesn't matter which is
    %% that the entropy grid is not exactly log spaced, however the error
    %% introduced by this is small
    
    global entropy_coeffs mass_coeffs flux_coeffs
    
    entropy_estind = (log10(entropy) - entropy_coeffs(2)) / entropy_coeffs(1);
    if(entropy_estind < 1)
        entropy_estind = 1.001;
    elseif(entropy_estind > length(grid.entropyVect))
        entropy_estind = length(grid.entropyVect)-0.001;
    end
    mass_estind = (log10(Mp/JUPITERMASS) - mass_coeffs(2)) / mass_coeffs(1);
    if(mass_estind < 1)
        mass_estind = 1.001;
    elseif(mass_estind > length(grid.MassVect))
        mass_estind = length(grid.MassVect)-0.001;
    end
    flux_estind = (log10(flux) - flux_coeffs(2)) / flux_coeffs(1);
    if(flux_estind < 1)
        flux_estind = 1.001;
    elseif(flux_estind > length(grid.fluxVect))
        flux_estind = length(grid.fluxVect)-0.001;
    end
    
    entropy_lowind = floor(entropy_estind);
    entropy_highind = entropy_lowind + 1;
    entropy_alpha = (entropy_estind - entropy_lowind);    
    
    mass_lowind = floor(mass_estind);
    mass_highind = mass_lowind + 1;
    mass_alpha = (mass_estind - mass_lowind);
    
    flux_lowind = floor(flux_estind);
    flux_highind = flux_lowind + 1;
    flux_alpha = (flux_estind - flux_lowind);
    
    wts3 = zeros(2,2,2);
    wts2 = zeros(2,2);
    
    %% i didn't want to figure out how to do this correctly with
    %% multiplication so i just did this
    wts3(1,1,1) = (1-mass_alpha)*(1-entropy_alpha) * (1-flux_alpha);
    wts3(1,1,2) = (1-mass_alpha)*(1-entropy_alpha) * (flux_alpha);
    wts3(1,2,1) = (1-mass_alpha)*(entropy_alpha) * (1-flux_alpha);
    wts3(1,2,2) = (1-mass_alpha)*(entropy_alpha) * (flux_alpha);
    wts3(2,1,1) = (mass_alpha)*(1-entropy_alpha) * (1-flux_alpha);
    wts3(2,1,2) = (mass_alpha)*(1-entropy_alpha) * (flux_alpha);
    wts3(2,2,2) = (mass_alpha)*(entropy_alpha) * (flux_alpha);
    
    wts2(1,1) = (1-mass_alpha)*(1-entropy_alpha);
    wts2(1,2) = (1-mass_alpha)*(entropy_alpha);
    wts2(2,1) = (mass_alpha)*(1-entropy_alpha);
    wts2(2,2) = (mass_alpha)*(entropy_alpha);
    
    function val = dot3(A,B)
        val = A(1,1,1)*B(1,1,1) ...
            + A(1,1,2)*B(1,1,2) ...
            + A(1,2,1)*B(1,2,1) ...
            + A(1,2,2)*B(1,2,2) ...
            + A(2,1,1)*B(2,1,1) ...
            + A(2,1,2)*B(2,1,2) ...
            + A(2,2,2)*B(2,2,2) ;
    end
    
    function val = dot2(A,B)
        val = A(1,1)*B(1,1) ...
            + A(1,2)*B(1,2) ...
            + A(2,1)*B(2,1) ...
            + A(2,2)*B(2,2) ;
    end

    Teff = dot3(grid.Teff(mass_lowind:mass_highind, entropy_lowind:entropy_highind, flux_lowind:flux_highind), wts3);
    Rp = dot2(grid.PlanetRadius(mass_lowind:mass_highind, entropy_lowind:entropy_highind), wts2);
    Ipl = dot2(grid.MomentOfInertia(mass_lowind:mass_highind, entropy_lowind:entropy_highind), wts2);
    Lpl = dot3(grid.LumInt(mass_lowind:mass_highind, entropy_lowind:entropy_highind, flux_lowind:flux_highind), wts3);
    dEds = dot2(grid.dEds(mass_lowind:mass_highind, entropy_lowind:entropy_highind), wts2);
    
    epsst = 1; %% assume the whole star takes part in tidal evolution
    epspl = 1; %% as well as the planet
    
    alphast = 0.1 ; %% assume centrally condensed
    alphapl = Ipl / (Mp * Rp^2) ; 
    
    mu = G * (Mp + Ms);
    Period = 2 * pi * sqrt(a^3/mu);
    n = 2 * pi / Period;
    
    f1 = (1 + (15/4)*e^2 + (15/8)*e^4 + (5/64)*e^6) / (1 - e^2)^6.5;
    f2 = (1 + (3/2)*e^2 + (1/8)*e^4) / (1 - e^2)^5;
    
    gp = ((81/2)*(n * e)/Qp)*(Ms/Mp)*(Rp/a)^5*(-f1+(11/18)*f2*Omegap/n);
    gs = ((81/2)*(n * e)/Qs)*(Mp/Ms)*(Rs/a)^5*(-f1+(11/18)*f2*Omegap/n);
    
    dedt = gp + gs;
    
    dwstdt = -0.5 * Omegas / (t + 1e7*YR); 
    dwpldt = 0;
    
    f3 = (1 + 7.5*e^2+(45/8)*e^4 + (5/16)*e^6)/ (1-e^2)^6;
    f4 = (1 + 3*e^2+(3/8)*e^4) / (1 - e^2)^4.5;
    
    dOmegasdt = (9/2)*(n^2 / (epsst * alphast * Qs)) * (Mp/Ms)^2 * (Rs / a)^3 * (f3 - f4*(Omegas/n)) + dwstdt;
    dOmegapdt = (9/2)*(n^2 / (epspl * alphapl * Qp)) * (Ms/Mp) * (Rp/a)^3 * (f3 - f4*(Omegap/n)) + dwpldt;
    
    Ptidal = (G * Ms * Mp * e * abs(dedt)) / (a * (1 - e^2)) + Ipl * Omegap * abs(dOmegapdt) ;
    
    dentropydt = (1 / dEds) * (- Lpl + Ptidal);

    %% this just includes effect from tides doesn't yet include mass loss
    dadt_tides = (2*a)*(e*dedt-4.5*((1/Qp)*(Ms/Mp)*(Rp/a)^5*(n-Omegap) + (1/Qs)*(Mp/Ms)*(Rs/a)^5*(n-Omegas)));

    
    %% TODO: this currently assumes that the mass is constantly  being
    %% lost during the entire orbit.  For highly eccentric orbits, these
    %% equations probably need to be somewhat adjusted.  
    
    
    %% Figure out how much mass is lost here.  Include the effect on dadt.
    alpha = (Mp / (3 * Ms))^(1/3);
    cs = sqrt(1.66 * k * Teff / (mu * mH));
    %% These are useful if you want to figure out if mass is being lost
    %% also through L2
%    hp = cs^2 / (4 * alpha^1.5 * G * Ms / a^2);
%    DelD = (2*a/3)*(alpha^1.5 + alpha^2);
    P0 = 0.1 * 1e6; 
    rho0 = (mu * mH * P0) / (k * Teff);
    
    K1 = (k * Teff * a) / (mu * mH * G * Ms);
    U0 = potenL1(Mp, Ms, a, Rp);
    K2 = K1 * log(rho0) - U0;
    
    zL1 = (alpha - alpha^2 / 3) * a;
    zL2 = (alpha + alpha^2 / 3) * a;
    UL1 = potenL1(Mp, Ms, a, zL1);
    UL2 = potenL2(Mp, Ms, a, zL2);
    
    rhoL1 = exp((UL1 + K2)/ K1);
    %rhoL2 = exp((UL2 + K2) / K1);
    
    dMdt_L1 = - ((2 * pi * rhoL1 * a^3 * cs^3) / (3 * G * Ms)) * (1 - exp(- (2*G*Mp) / (3*a*cs^2)));
    dadt_ml = -2. * dMdt_L1 * a / Mp;
    
    dMdt_evap = - 0.1 * Lxuv * Rp^3 / (G * Mp * a^2);
    
    dadt = dadt_tides + dadt_ml;
    dMpdt = dMdt_L1  + dMdt_evap;
    
    dy(1) = dadt;
    dy(2) = dedt;
    dy(3) = dMpdt;
    dy(4) = dentropydt;
    dy(5) = dOmegapdt;
    dy(6) = dOmegasdt;
end