function dy = stationary_ev(t, y, params)
    function U1 = potenL1(Mp, Ms, a, z)
        r1 = (a - z) / a;
        r2 = z / a;
        U1 = (1 - Mp / Ms) * (1 / r1 + r1^2 / 2) + (Mp / Ms) * (1 / r2 + r2^2 / 2);
    end

    function U2 = potenL2(Mp, Ms, a, z)
        r1 = (a + z) / a;
        r2 = z / a;
        U2 = (1 - Mp / Ms) * (1 / r1 + r1^2 / 2) + (Mp / Ms) * (1 / r2 + r2^2 / 2);
    end

    %% These commands should be run before calling this function such that
    %% these variables are already defined
    %global grid %% declare the planet grid to be a global variable.  
                 %% i don't want this to be loaded multiple times
    global AU EARTHMASS G JUPITERMASS JUPITERRADIUS SIGMA SOLARMASS SOLARRADIUS YR sconv
    %%% these constants should be delcared global and loaded with 
    %%    load solar_system_constants.mat
    %%% In the controlling file
    k = 1.380658e-16;
    mH = 1.67e-24;
    
    dy = zeros(1,1); %%  a , e, Mp, s, Omegap, Omegas
    %% values are generally in cgs and integrated in cgs.
    entropy = y(1) ; %% planet entropy
    
    Ls = params(1) ;
    a  = params(2) ;
    Mp = params(3) ;
    
    flux = Ls / (4 * pi * a^2);
    if(entropy > ((log10(Mp / JUPITERMASS) + 4.32) / 0.39)) %% this was an empirically determined relation where 
        warning('Planets mass/entropy is in the messed up zone - OK as long as it moves out quick.'); %#ok<WNTAG>
        disp(entropy);
        disp(Mp/JUPITERMASS);
        disp(t/YR);
    end

    [Teff, Rp, Ipl, Lpl, dEds] = get_planet_properties(Mp, entropy, flux);
    
    dentropydt = (1 / dEds) * (- Lpl);

    dy(1) = dentropydt;
end