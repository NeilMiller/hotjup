function output = load_core(filename)
    [pressure, density] = textread(filename, '%f %f');
    arraysize = size(pressure,1);
    output = struct('pressure', pressure, 'density', density, 'arraysize', arraysize);
end