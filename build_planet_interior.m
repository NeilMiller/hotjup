function my_planet = build_planet_interior(adiabats, adiabatindex, coreeos, planet_massJ, core_massE)
    %% This function will build hydrostatic equilibrium structures given
    %% a choice of adiabat and core equation of state as well as the planet
    %% mass and core mass.  It then uses the boundary condition table
    %% already computed to try to determine the net luminosity of the
    %% planet.  Other properties like the moment of inertia are also
    %% computed
    load solar_system_constants.mat
    load std_bound.mat
    gridsize = 1001;
    numflux = 100.;
    
    my_planet = struct('mass', planet_massJ * JUPITERMASS, 'coremass', core_massE * EARTHMASS, ...
                       'corefrac', (core_massE * EARTHMASS) / (planet_massJ * JUPITERMASS), ...
                       'CZbase', 0, 'masstab', zeros(gridsize,1), 'pressuretab', zeros(gridsize,1), ...
                       'temperaturetab', zeros(gridsize,1), 'radiustab', zeros(gridsize,1), 'densitytab', zeros(gridsize,1), ...
                       'dmtab', zeros(gridsize,1), 'gridsize', gridsize, 'radius', 0., ...
                       'entropy', adiabats.entropy_vect(adiabatindex), 'gravity', 0., ...
                       'tempeffective', zeros(numflux, 1), 'tempintrinsic', zeros(numflux, 1), ...
                       'temp0', 0., 'Luminosity', zeros(numflux, 1), ...
                       'atmosphereHeight', zeros(numflux,1), 'incidentflux', zeros(numflux,1), ...
                       'error', 0, 'MomentOfInertia', 0, 'dEds', 0);
                 
    if(my_planet.coremass > my_planet.mass)
        my_planet.error = 10;
        error('Planet mass smaller than core mass');
    end    
    if(my_planet.coremass < 0) 
        my_planet.error = 10;
        error('Planet core mass is negative');
    end
    if(my_planet.mass < 0)
        my_planet.error = 10;
        error('Planet mass is negative');
    end
    
    %% 1. Guess initial profile
    my_planet.masstab = ((-cos(pi * (0:(gridsize-1)) / (gridsize-1)) + 1)*0.5*my_planet.mass)' ;
    my_planet.dmtab(1:(gridsize-1)) = my_planet.masstab(2:gridsize) - my_planet.masstab(1:(gridsize-1));
    my_planet.CZbase = find(my_planet.masstab < my_planet.coremass, 1, 'last' );
    if(my_planet.coremass == 0) 
        my_planet.CZbase = -1;
    end
    
    GuessDensity = 1;
    
    my_planet.radiustab = (my_planet.masstab / (GuessDensity * 4 * pi / 3)).^(1./3.);
    my_planet.densitytab(1:gridsize) = GuessDensity;

    logPc = log10(coreeos.pressure);
    logRhoc = log10(coreeos.density);
    logP = log10(adiabats.pressure_mat(adiabatindex,:));
    logRho = log10(adiabats.density_mat(adiabatindex,:));
    logT = log10(adiabats.temp_mat(adiabatindex,:));
    
    %% 2. Iterate computing hydrostatic equilibrium 
    for i=1:500 
        %% a. find hydrostatic equilibrium pressure
        my_planet.pressuretab(gridsize) = 1;
        for j=(gridsize-1):-1:2
            my_planet.pressuretab(j) = (my_planet.pressuretab(j+1)+2.*G*(my_planet.masstab(j+1)^2 - my_planet.masstab(j)^2) ...
                                        / (pi * (my_planet.radiustab(j+1) + my_planet.radiustab(j))^4));
            if(isnan(my_planet.pressuretab(j)))
          %      my_planet.pressuretab
          %      (my_planet.pressuretab(j+1) + 2.*G*(my_planet.masstab(j+1)^2 - my_planet.masstab(j)^2))
          %      pi * (my_planet.radiustab(j+1) + my_planet.radiustab(j))^4
              %  j 
          %       my_planet.radiustab(j+1)
          %       my_planet.radiustab(j)
              %  y = input('isnan occuring here');
               error('isnan occuring');
                
            end
        end
        my_planet.pressuretab(1) = my_planet.pressuretab(2) + (4 * pi / 6) * G * (my_planet.densitytab(1)*my_planet.radiustab(2))^2;
        %% b. find density and radius 
        
        if( my_planet.CZbase >= 1)            
 %           my_planet.densitytab = [interp1(coreeos.pressure, coreeos.density, my_planet.pressuretab(1:my_planet.CZbase)') ...
 %                                   interp1(adiabats.pressure_mat(adiabatindex,:), adiabats.density_mat(adiabatindex,:), ...
 %                                           my_planet.pressuretab((my_planet.CZbase+1):gridsize)')]';
            my_planet.densitytab = 10.^([interp1(logPc, logRhoc, log10(my_planet.pressuretab(1:my_planet.CZbase)'), 'linear', 'extrap') ...
                                         interp1(logP, logRho, log10(my_planet.pressuretab((my_planet.CZbase+1):gridsize)'), 'linear', 'extrap')]);
        else
            
            my_planet.densitytab = interp1(adiabats.pressure_mat(adiabatindex,:), adiabats.density_mat(adiabatindex,:), ...
                                            my_planet.pressuretab);
        end
        my_planet.densitytab(gridsize) = 0;
        
        my_planet.radiustab(1) = 0;
        for j=2:gridsize
            dv = my_planet.dmtab(j-1) / ((my_planet.densitytab(j) + my_planet.densitytab(j-1))/2);
            my_planet.radiustab(j) = (my_planet.radiustab(j-1)^3 + (3 / (4*pi)) * dv)^(1/3);
            
            if(isnan(dv)) 
              %  dv
              %  j
           %     my_planet.radiustab(j-1)
           %     my_planet.radiustab(j)
           %     my_planet.dmtab(j-1)
           %     my_planet.densitytab(j)
                %y = input('isnan occuring here');
                error('isnan occuring');
            end
        end        
    end
    
    %% 3. wrap up, calculate other stuff after hydrostatic equilibrium 
    %% Determine the gravity of the planet's surface
%    my_planet.radius = my_planet.radiustab(gridsize);
    my_planet.radius = interp1(my_planet.pressuretab, my_planet.radiustab, std_bound.pressure0);
    my_planet.gravity = G * my_planet.mass / (my_planet.radius)^2;
    my_planet.MomentOfInertia = (2./3.) * my_planet.dmtab(1:(gridsize-1))' * (my_planet.radiustab(2:gridsize).^2);    
    
    if(my_planet.CZbase >= 1) 
%        my_planet.temperaturetab((my_planet.CZbase+1):gridsize) = interp1(adiabats.pressure_mat(adiabatindex,:), ...
%                        adiabats.temp_mat(adiabatindex,:),my_planet.pressuretab((my_planet.CZbase+1):gridsize));
        my_planet.temperaturetab((my_planet.CZbase+1):gridsize) = 10.^interp1(logP, logT, log10(my_planet.pressuretab((my_planet.CZbase+1):gridsize)'), 'linear', 'extrap');
        my_planet.temperaturetab(1:my_planet.CZbase) = my_planet.temperaturetab(my_planet.CZbase+1);        
    else
%        my_planet.temperaturetab = interp1(adiabats.pressure_mat(adiabatindex,:), adiabats.temp_mat(adiabatindex,:), my_planet.pressuretab);
        my_planet.temperaturetab = 10.^interp1(logP, logT, log10(my_planet.pressuretab'), 'linear', 'extrap');
    end
    my_planet.temperaturetab(gridsize) = 0;
    
    my_planet.dEds = my_planet.temperaturetab((my_planet.CZbase+1):gridsize)' * my_planet.dmtab((my_planet.CZbase+1):gridsize) * sconv ;
        
    %% 4. Attach boundary to planet.
    %% Check that planet's gravity is inside of grid
    if((my_planet.gravity > max(std_bound.gravity_list)) || (my_planet.gravity < min(std_bound.gravity_list)))
        warning('Input gravity outside of standard boundary table range.'); %#ok<WNTAG>
        my_planet.error = 15;
        return
    end
    
    %% check that pressure is on the grid
    if ((min(my_planet.pressuretab) > std_bound.pressure0) || (max(my_planet.pressuretab) < std_bound.pressure0))
        warning('pressure0 is not inside the range of pressure values inside the planet.  Can not attach boundary'); %#ok<WNTAG>
        my_planet.error = 16;
        return
    end
    my_planet.temp0 = interp1(adiabats.pressure_mat(adiabatindex,:), adiabats.temp_mat(adiabatindex,:), std_bound.pressure0) ;
    
    %% adjust radius so that it is located at the pressure 0 level
    my_planet.radius = interp1(my_planet.pressuretab, my_planet.radiustab, std_bound.pressure0);
    
    [Xg, Yg, Zg] = ndgrid(log10(std_bound.flux_list), log10(std_bound.gravity_list), std_bound.tempint_list);
    
    my_planet.incidentflux = log_grid(min(std_bound.flux_list)*1.0001, max(std_bound.flux_list)*0.999, numflux);
    
    for i=1:numflux %% This might be slightly less effecient, but hopefully easier to understand
        Ntemp = size(std_bound.tempint_list,2);

        [xi , yi, zi] = ndgrid(log10(my_planet.incidentflux(i)), log10(my_planet.gravity), std_bound.tempint_list);
        
        tmp_temp0_list = reshape(interp3(Xg, Yg, Zg, std_bound.temp0_table, ...
                                 xi, yi, zi), Ntemp,1);
        tmp_tempeff_list = reshape(interp3(Xg, Yg, Zg, std_bound.temp_effective_table, ...
                                   xi,yi, zi) , Ntemp,1);
        
        if(max(tmp_temp0_list) < my_planet.temp0)
            warning('Planets temperature is larger than grid - setting temperature to max'); %#ok<WNTAG>
            planet.tempintrinsic(i) = max(std_bound.tempint_list);
            my_planet.tempeffective(i) = max(tmp_tempeff_list);
        elseif(min(tmp_temp0_list) > my_planet.temp0)
            warning('Planets temperature is smaller than grid - setting temperature to min'); %#ok<WNTAG>
            planet.tempintrinsic(i) = min(std_bound.tempint_list);
            my_planet.tempeffective(i) = min(tmp_tempeff_list);
        else
            my_planet.tempintrinsic(i) = interp1(tmp_temp0_list, std_bound.tempint_list, my_planet.temp0);
            my_planet.tempeffective(i) = interp1(tmp_temp0_list, tmp_tempeff_list, my_planet.temp0);
        end
        
        my_planet.Luminosity(i) = 4 * pi * my_planet.radius^2 * SIGMA * (my_planet.tempintrinsic(i)^4);
        my_planet.atmosphereHeight(i) = 10^(8.74) * my_planet.tempeffective(i) / my_planet.gravity;
    end
end