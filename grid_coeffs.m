function coeffs = grid_coeffs(logsp_grid)
    N = length(logsp_grid);
    a = (log10(logsp_grid(1)) - log10(logsp_grid(N))) / (1 - N);
    b = log10(logsp_grid(1)) - a ;
    
    coeffs = [a b];
end