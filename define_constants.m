function define_constants
    G =  6.67259e-8;        % dyne cm^2 g^-2
    AU = 1.496e13;          % cm
    EARTHMASS = 5.974e27;   % g
    JUPITERMASS = 317.894 * 5.974d27; % grams
    JUPITERRADIUS = 11.19 * 6.378d8;  % cm
    SOLARMASS = 1.989d33; % grams
    SOLARRADIUS = 6.9599d10; % cm
    YR = 31536000.0;  %seconds
    sconv=8.2495514d7;  % I have no idea what units these are
    SIGMA = 5.670514e-5;

    save('solar_system_constants.mat', 'G', 'AU', 'EARTHMASS', ...
         'JUPITERMASS', 'JUPITERRADIUS', 'SOLARMASS', 'SOLARRADIUS', ...
         'YR','sconv', 'SIGMA');
     
    
end