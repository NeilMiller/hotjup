function grid = build_planet_grid(coremassE)
    load solar_system_constants.mat
    adiabats = load_adiabats('eos_data/HHe_adiabats.dat');
    coreeos = load_core('eos_data/aneos_50rock50ice.dat');
    Nentropy = adiabats.numprofiles;
    Nmass = 100;
    Nflux = 100;
    MinMassJ = max([coremassE * EARTHMASS / JUPITERMASS, 0.06]);
    MaxMassJ = 10;
    MassVect = log_grid(MinMassJ, MaxMassJ, Nmass);

    grid = struct('MassVect', MassVect, ...
                  'entropyVect', adiabats.entropy_vect, ...
                  'fluxVect', zeros(Nflux,1), ...
                  'Teff', zeros(Nmass, Nentropy, Nflux), ...
                  'dEds', zeros(Nmass, Nentropy), ...
                  'LumInt', zeros(Nmass, Nentropy, Nflux), ...
                  'PlanetRadius', zeros(Nmass, Nentropy), ...
                  'TransitRadius', zeros(Nmass, Nentropy, Nflux), ...
                  'error', zeros(Nmass, Nentropy), ...
                  'MomentOfInertia', zeros(Nmass, Nentropy, Nflux));
    
    for mass_ind = 1:Nmass
        for adiabat_ind = 1:Nentropy
            pl = build_planet_interior(adiabats, adiabat_ind, coreeos, ...
                                       MassVect(mass_ind), coremassE);
            grid.dEds(mass_ind, adiabat_ind) = pl.dEds;
            grid.PlanetRadius(mass_ind, adiabat_ind) = pl.radius;
            grid.error(mass_ind, adiabat_ind) = pl.error;
            grid.MomentOfInertia(mass_ind, adiabat_ind) = pl.MomentOfInertia;
            for flux_ind = 1:Nflux
                grid.Teff(mass_ind, adiabat_ind, flux_ind) = pl.tempeffective(flux_ind);
                grid.LumInt(mass_ind, adiabat_ind, flux_ind) = pl.Luminosity(flux_ind);
                grid.TransitRadius(mass_ind, adiabat_ind, flux_ind) = pl.radius + pl.atmosphereHeight(flux_ind);
            end
        end
    end
    grid.fluxVect = pl.incidentflux;
end