function pl = test_build_planet
    adiabats = load_adiabats('eos_data/HHe_adiabats.dat');
    save('eos_data/HHe_adiabats.mat', 'adiabats');
    coreeos = load_core('eos_data/aneos_50rock50ice.dat');
    save('eos_data/coreeos.mat', 'coreeos');
    adiabatindex = 1;
    planet_massJ = 5;
    core_massE = 4.;
    
    pl = build_planet_interior(adiabats, adiabatindex, coreeos, planet_massJ, core_massE);
end