function entropy = find_entropy_inflated(planet_mass, planet_radius, incident_flux)
    load planet_grid_10ME.mat
    [X,Y,Z] = ndgrid(grid.MassVect', grid.entropyVect, grid.fluxVect');
    [xi,yi,zi] = ndgrid(planet_mass, grid.entropyVect, incident_flux);
    tmp_rad_vect = reshape(interp3(X,Y,Z, grid.TransitRadius, ...
                               xi, yi, zi), size(grid.entropyVect,1), 1);
    
    entropy = interp1(tmp_rad_vect, grid.entropyVect, planet_radius);    
end