function grid = build_planet_grid_adv(coremassE)
    % This is the "advanced" version of the code - 
    %% the previous version did not converge under some circumstances of
    %% either high entropy or low mass.  The main difference here is that I
    %% intend to have the code use the previous temperature - density
    %% profile as a guess.
    
    %% Algorithm: Start at the high mass, low entropy corner of the grid.
    %%    then move to higher entropies for the constant mass
    %%     at the next mass, use the temperature and density profile from
    %%     the corresponding profile at marginally higher mass
    %%   Note: this requires having two "old" profiles on hand.  The one
    %%   that corresponds to the current mass of planet, just the last
    %%   entropy as well as the one that corresponds to the low entropy
    %%   previous mass computation
    
    
    load solar_system_constants.mat
    load std_bound.mat
    load eos_data/HHe_adiabats.mat
    load eos_data/coreeos.mat
    %adiabats = load_adiabats('eos_data/HHe_adiabats.dat');
    %coreeos = load_core('eos_data/aneos_50rock50ice.dat');
    Nentropy = adiabats.numprofiles;
    Nmass = 20;
    Nflux = 100;
    MinMassJ = (coremassE + 0.3) * EARTHMASS / JUPITERMASS;
    MaxMassJ = 10;
    MassVect = log_grid(MinMassJ, MaxMassJ, Nmass);

    gridsize = 1001;
    numflux = 100.;
    
    grid = struct('MassVect', MassVect, ...
                  'entropyVect', adiabats.entropy_vect, ...
                  'fluxVect', zeros(Nflux,1), ...
                  'Teff', zeros(Nmass, Nentropy, Nflux), ...
                  'dEds', zeros(Nmass, Nentropy), ...
                  'LumInt', zeros(Nmass, Nentropy, Nflux), ...
                  'PlanetRadius', zeros(Nmass, Nentropy), ...
                  'TransitRadius', zeros(Nmass, Nentropy, Nflux), ...
                  'error', zeros(Nmass, Nentropy), ...
                  'MomentOfInertia', zeros(Nmass, Nentropy));
   
    my_planet = struct('mass', 0, 'coremass', 0, ...
                       'corefrac', 0, ...
                       'CZbase', 0, 'masstab', zeros(gridsize,1), 'pressuretab', zeros(gridsize,1), ...
                       'temperaturetab', zeros(gridsize,1), 'radiustab', zeros(gridsize,1), 'densitytab', zeros(gridsize,1), ...
                       'dmtab', zeros(gridsize,1), 'gridsize', gridsize, 'radius', 0., ...
                       'entropy', 0, 'gravity', 0., ...
                       'tempeffective', zeros(numflux, 1), 'tempintrinsic', zeros(numflux, 1), ...
                       'temp0', 0., 'Luminosity', zeros(numflux, 1), ...
                       'atmosphereHeight', zeros(numflux,1), 'incidentflux', zeros(numflux,1), ...
                       'error', 0, 'MomentOfInertia', 0, 'dEds', 0);
    
     useOLD = 0;
                   
%    last_planet_entropy = my_planet;  %% equal mass, 1 entropy unit smaller
%    last_planet_mass = my_planet;     %% equal entropy, one mass unit larger
    for mass_ind = Nmass:-1:1
        for adiabatindex = Nentropy:-1:1
            %% HERE IS THE MEAT
            planet_massJ = MassVect(mass_ind);
    %        function my_planet = build_planet_interior(adiabats, adiabatindex, coreeos, planet_massJ, coremassE)
    %% This function will build hydrostatic equilibrium structures given
    %% a choice of adiabat and core equation of state as well as the planet
    %% mass and core mass.  It then uses the boundary condition table
    %% already computed to try to determine the net luminosity of the
    %% planet.  Other properties like the moment of inertia are also
    %% computed
    %load solar_system_constants.mat
    %load std_bound.mat
    %gridsize = 301;
    %numflux = 100.;
    
%    my_planet = struct('mass', planet_massJ * JUPITERMASS, 'coremass', coremassE * EARTHMASS, ...
%                       'corefrac', (coremassE * EARTHMASS) / (planet_massJ * JUPITERMASS), ...
%                       'CZbase', 0, 'masstab', zeros(gridsize,1), 'pressuretab', zeros(gridsize,1), ...
%                       'temperaturetab', zeros(gridsize,1), 'radiustab', zeros(gridsize,1), 'densitytab', zeros(gridsize,1), ...
%                       'dmtab', zeros(gridsize,1), 'gridsize', gridsize, 'radius', 0., ...
%                       'entropy', adiabats.entropy_vect(adiabatindex), 'gravity', 0., ...
%                       'tempeffective', zeros(numflux, 1), 'tempintrinsic', zeros(numflux, 1), ...
%                       'temp0', 0., 'Luminosity', zeros(numflux, 1), ...
%                       'atmosphereHeight', zeros(numflux,1), 'incidentflux', zeros(numflux,1), ...
%                       'error', 0, 'MomentOfInertia', 0, 'dEds', 0);
    
            if(useOLD == 1) 
                my_planet = build_planet_interior(adiabats, adiabatindex, coreeos, planet_massJ, coremassE);
                grid.dEds(mass_ind, adiabatindex) = my_planet.dEds;  % set these now incase the program fails further on
                grid.PlanetRadius(mass_ind, adiabatindex) = my_planet.radius;
                grid.MomentOfInertia(mass_ind, adiabatindex) = my_planet.MomentOfInertia;
                grid.error(mass_ind, adiabatindex) = my_planet.error;
            else
                my_planet.mass = planet_massJ * JUPITERMASS;
                my_planet.coremass = coremassE * EARTHMASS;
                my_planet.corefrac = (coremassE * EARTHMASS) / (planet_massJ * JUPITERMASS);
                my_planet.entropy = adiabats.entropy_vect(adiabatindex) ;

                if(my_planet.coremass > my_planet.mass)         
                    my_planet.error = 10;
                    error('Planet mass smaller than core mass');
                end    
                if(my_planet.coremass < 0) 
                    my_planet.error = 10;
                    error('Planet core mass is negative');
                end
                if(my_planet.mass < 0)
                    my_planet.error = 10;
                    error('Planet mass is negative');
                end
            
            %% 1a setup mass profile 
                my_planet.masstab = ((-cos(pi * (0:(gridsize-1)) / (gridsize-1)) + 1)*0.5*my_planet.mass)' ;
                my_planet.dmtab(1:(gridsize-1)) = my_planet.masstab(2:gridsize) - my_planet.masstab(1:(gridsize-1));
                my_planet.dmtab(gridsize) = my_planet.dmtab(gridsize-1);
                my_planet.CZbase = find(my_planet.masstab < my_planet.coremass, 1, 'last' );
                if(my_planet.coremass == 0) 
                    my_planet.CZbase = -1;
                end

                %% 1b Guess initial profile
    %            if((mass_ind == Nmass) && (adiabatindex == Nentropy))
                    %%% This is the very first profile.  Use the guess
                    %'Cold start'
                    GuessDensity = 1.;

                    my_planet.radiustab = (my_planet.masstab / (GuessDensity * 4 * pi / 3)).^(1./3.);
                    my_planet.densitytab(1:gridsize) = GuessDensity;
    %            elseif(adiabatindex == Nentropy) 
                    %% This is a new row
    %                my_planet.radiustab = last_planet_mass.radiustab;
    %                my_planet.densitytab = last_planet_mass.densitytab;
    %            else
    %                my_planet.radiustab = last_planet_entropy.radiustab;
    %                my_planet.densitytab = last_planet_entropy.densitytab;
    %            end


                logPc = log10(coreeos.pressure);
                logRhoc = log10(coreeos.density);
                logP = log10(adiabats.pressure_mat(adiabatindex,:));
                logRho = log10(adiabats.density_mat(adiabatindex,:));
                logT = log10(adiabats.temp_mat(adiabatindex,:));

                %% 2. Iterate computing hydrostatic equilibrium 
                for i=1:500 
                    %% a. find hydrostatic equilibrium pressure

                    my_planet.pressuretab(gridsize) = 1;
                    for j=(gridsize-1):-1:2
                        my_planet.pressuretab(j) = (my_planet.pressuretab(j+1)+2.*G*(my_planet.masstab(j+1)^2 - my_planet.masstab(j)^2) ...
                                            / (pi * (my_planet.radiustab(j+1) + my_planet.radiustab(j))^4));
                    end
                    my_planet.pressuretab(1) = my_planet.pressuretab(2) + (4 * pi / 6) * G * (my_planet.densitytab(1)*my_planet.radiustab(2))^2;
                    %% b. find density and radius 

                    if( my_planet.CZbase >= 1)       
                        my_planet.densitytab = 10.^([interp1(logPc, logRhoc, log10(my_planet.pressuretab(1:my_planet.CZbase)'), 'linear', 'extrap') ...
                                             interp1(logP, logRho, log10(my_planet.pressuretab((my_planet.CZbase+1):gridsize)'), 'linear', 'extrap')]);
                        %my_planet.densitytab = [interp1(coreeos.pressure, coreeos.density, my_planet.pressuretab(1:my_planet.CZbase))' ...
                        %                        interp1(adiabats.pressure_mat(adiabatindex,:), adiabats.density_mat(adiabatindex,:), ...
                        %                                my_planet.pressuretab((my_planet.CZbase+1):gridsize))']';
                    else
                        %my_planet.densitytab = interp1(adiabats.pressure_mat(adiabatindex,:), adiabats.density_mat(adiabatindex,:), ...
                        %                               my_planet.pressuretab);
                        my_planet.densitytab = 10.^interp1(logP, logRho, log10(my_planet.pressuretab'), 'linear', 'extrap');
                    end
                    my_planet.densitytab(gridsize) = 0;

                    my_planet.radiustab(1) = 0;
                    for j=2:gridsize
                        dv = my_planet.dmtab(j-1) / ((my_planet.densitytab(j) + my_planet.densitytab(j-1))/2);
                        my_planet.radiustab(j) = (my_planet.radiustab(j-1)^3 + (3 / (4*pi)) * dv)^(1/3);
                    end        

                    %plot(my_planet.masstab, my_planet.pressuretab);
                    %log10(my_planet.dmtab/my_planet.mass)
                    %my_planet.pressuretab
                    %x = input('Next Iteration');
                end
    %            plot(my_planet.masstab, my_planet.pressuretab);
    %            x = input('next entropy');
    %            [adiabats.entropy_vect(adiabatindex), MassVect(mass_ind), my_planet.radius / JUPITERRADIUS]

                if(isnan(my_planet.radius)) 
                    warning('Failed to converge'); %#ok<WNTAG>
                    disp('Mass');
                    disp(my_planet.mass / JUPITERMASS);
                    my_planet.entropy
                    my_planet.error = 50;
                    grid.error(mass_ind, adiabatindex) = my_planet.error;
                end
      %          x = input('Continue?');
      %          if(x == 'n') 
      %              'Stopping'
      %              return
      %          end


                %% 3. wrap up, calculate other stuff after hydrostatic equilibrium 
                %% Determine the gravity of the planet's surface
                %my_planet.radius = my_planet.radiustab(gridsize);
                my_planet.radius = interp1(my_planet.pressuretab, my_planet.radiustab, std_bound.pressure0);
                my_planet.gravity = G * my_planet.mass / (my_planet.radius)^2;
                my_planet.MomentOfInertia = (2./3.) * my_planet.dmtab(1:(gridsize-1))' * (my_planet.radiustab(2:gridsize).^2);    

                if(my_planet.CZbase >= 1) 
    %                my_planet.temperaturetab((my_planet.CZbase+1):gridsize) = interp1(adiabats.pressure_mat(adiabatindex,:), ...
    %                        adiabats.temp_mat(adiabatindex,:),my_planet.pressuretab((my_planet.CZbase+1):gridsize));
                    my_planet.temperaturetab((my_planet.CZbase+1):gridsize) = 10.^interp1(logP, logT, log10(my_planet.pressuretab((my_planet.CZbase+1):gridsize)'), 'linear', 'extrap');
                    my_planet.temperaturetab(1:my_planet.CZbase) = my_planet.temperaturetab(my_planet.CZbase+1);        
                else
                    my_planet.temperaturetab = 10.^interp1(logP, logT, log10(my_planet.pressuretab'), 'linear', 'extrap');
    %                my_planet.temperaturetab = interp1(adiabats.pressure_mat(adiabatindex,:), adiabats.temp_mat(adiabatindex,:), my_planet.pressuretab);
                end
                my_planet.temperaturetab(gridsize) = 0;

                my_planet.dEds = my_planet.temperaturetab((my_planet.CZbase+1):gridsize)' * my_planet.dmtab((my_planet.CZbase+1):gridsize) * sconv ;

                grid.dEds(mass_ind, adiabatindex) = my_planet.dEds;  % set these now incase the program fails further on
                grid.PlanetRadius(mass_ind, adiabatindex) = my_planet.radius;
                grid.MomentOfInertia(mass_ind, adiabatindex) = my_planet.MomentOfInertia;

                %% 4. Attach boundary to planet.
                %% Check that planet's gravity is inside of grid
                if((my_planet.gravity > max(std_bound.gravity_list)) || (my_planet.gravity < min(std_bound.gravity_list)))
                    warning('Input gravity outside of standard boundary table range.'); %#ok<WNTAG>
                    my_planet.error = 15;
                    grid.error(mass_ind, adiabatindex) = my_planet.error;
                    continue
                end

                %% check that pressure is on the grid
                if ((min(my_planet.pressuretab) > std_bound.pressure0) || (max(my_planet.pressuretab) < std_bound.pressure0))
                    warning('pressure0 is not inside the range of pressure values inside the planet.  Can not attach boundary'); %#ok<WNTAG>
                    my_planet.error = 16;
                    grid.error(mass_ind, adiabatindex) = my_planet.error;
                    continue
                end
                my_planet.temp0 = interp1(adiabats.pressure_mat(adiabatindex,:), adiabats.temp_mat(adiabatindex,:), std_bound.pressure0) ;

                %% adjust radius so that it is located at the pressure 0 level
                my_planet.radius = interp1(my_planet.pressuretab, my_planet.radiustab, std_bound.pressure0);

                [Xg, Yg, Zg] = ndgrid(log10(std_bound.flux_list), log10(std_bound.gravity_list), std_bound.tempint_list);

                my_planet.incidentflux = log_grid(min(std_bound.flux_list)*1.0001, max(std_bound.flux_list)*0.999, numflux);

                for i=1:numflux %% This might be slightly less effecient, but hopefully easier to understand
                    Ntemp = size(std_bound.tempint_list,2);

                    [xi , yi, zi] = ndgrid(log10(my_planet.incidentflux(i)), log10(my_planet.gravity), std_bound.tempint_list);

                    tmp_temp0_list = reshape(interp3(Xg, Yg, Zg, std_bound.temp0_table, ...
                                     xi, yi, zi), Ntemp,1);
                    tmp_tempeff_list = reshape(interp3(Xg, Yg, Zg, std_bound.temp_effective_table, ...
                                       xi,yi, zi) , Ntemp,1);

                    if(max(tmp_temp0_list) < my_planet.temp0)
                        warning('Planets temperature is larger than grid - setting temperature to max'); %#ok<WNTAG>
                        planet.tempintrinsic(i) = max(std_bound.tempint_list);
                        my_planet.tempeffective(i) = max(tmp_tempeff_list);
                    elseif(min(tmp_temp0_list) > my_planet.temp0)
                        warning('Planets temperature is smaller than grid - setting temperature to min'); %#ok<WNTAG>
                        planet.tempintrinsic(i) = min(std_bound.tempint_list);
                        my_planet.tempeffective(i) = min(tmp_tempeff_list);
                    else
                        my_planet.tempintrinsic(i) = interp1(tmp_temp0_list, std_bound.tempint_list, my_planet.temp0);
                        my_planet.tempeffective(i) = interp1(tmp_temp0_list, tmp_tempeff_list, my_planet.temp0);
                    end

                    my_planet.Luminosity(i) = 4 * pi * my_planet.radius^2 * SIGMA * (my_planet.tempintrinsic(i)^4);
                    my_planet.atmosphereHeight(i) = 10^(8.74) * my_planet.tempeffective(i) / my_planet.gravity;
                end

                %% Save the last profile such that we can use a better initial
                %% condition
    %            if(adiabatindex == Nentropy)
    %                last_planet_mass = my_planet;
    %                last_planet_entropy = my_planet;
    %            else
    %                last_planet_entropy = my_planet;
    %            end

            %end

            end
                %% MEAT END
    %            pl = build_planet_interior(adiabats, adiabatindex, coreeos, ...
%                                       MassVect(mass_ind), coremassE);
            grid.error(mass_ind, adiabatindex) = my_planet.error;
            grid.MomentOfInertia(mass_ind, adiabatindex) = my_planet.MomentOfInertia;
            for flux_ind = 1:Nflux
                grid.Teff(mass_ind, adiabatindex, flux_ind) = my_planet.tempeffective(flux_ind);
                grid.LumInt(mass_ind, adiabatindex, flux_ind) = my_planet.Luminosity(flux_ind);
                grid.TransitRadius(mass_ind, adiabatindex, flux_ind) = my_planet.radius + my_planet.atmosphereHeight(flux_ind);
            end
        end
    end
    grid.fluxVect = my_planet.incidentflux;
end