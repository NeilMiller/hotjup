function [T,Y] = test_tidal_evolution

    global grid %% declare the planet grid to be a global variable.  
                 %% i don't want this to be loaded multiple times
    global AU EARTHMASS G JUPITERMASS JUPITERRADIUS SIGMA SOLARMASS SOLARRADIUS YR sconv
    
    global entropy_coeffs mass_coeffs flux_coeffs
    
    load solar_system_constants.mat
    load planet_grid_10ME.mat
    
    entropy_coeffs = grid_coeffs(grid.entropyVect);
    mass_coeffs = grid_coeffs(grid.MassVect);
    flux_coeffs = grid_coeffs(grid.fluxVect);
    
    Ms = SOLARMASS;
    Ls = 3.826e33;
    Rs = SOLARRADIUS;
    Qp = 1e5;
    Qs = 1e9;
    params = [Ms Ls Rs Qp Qs];
    
    
    
    a = 0.03 * AU ; %% semi-major axis
    e = 0.2 ; %% eccentricity
    Mp = JUPITERMASS ; %% planet mass
   % entropy = min([(log10(Mp / JUPITERMASS) + 4.32) / 0.39 - 0.1, max(grid.entropyVect)-0.1]) ; %% planet entropy
    entropy = 9.;
    Omegap = 0.0002 ; %% planet spin rate
    Omegas = 0.0005 ; %% Stellar spin rate
    y0 = [a e Mp entropy Omegap Omegas];
    
    
    options = odeset('MaxStep', 1e4 * YR, 'RelTol', 1e-4);
    tspan = [0, 1e8 * YR];
    [T0,Y0] = ode45(@(t,y)tidal_ev2(t,y,params), tspan, y0, options);
    L0 = size(T0,1);
    y1 = Y0(L0,:);
    options = odeset('MaxStep', 1e5 * YR, 'RelTol', 1e-4);
    tspan = [1e8*YR, 1e9 * YR];
    [T1,Y1] = ode45(@(t,y)tidal_ev2(t,y,params), tspan, y1, options);
    T = [T0;T1];
    Y = [Y0;Y1];
end