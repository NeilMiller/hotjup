function make_standard_boundary_table
    %% Define private functions
    %% load_sub_boundary reads the planet atmosphere boundary results from
    %% a standard fortney output file.  Each of these files have been
    %% computed at varying temperature and gravity for a fixed distance
    function output=load_sub_boundary(filename, flux, if_ngrav, if_ntemp, Pressure0)
        TEMPGRIDSIZE = 161;
        
        output=struct('gravity_list',zeros(if_ngrav,1), 'tempint_list', (0:(TEMPGRIDSIZE-1))*10., ...
                      'num_grav', if_ngrav, 'num_temp', TEMPGRIDSIZE, 'pressure0', Pressure0 * 1e6, ...
                      'tempefftab', zeros(if_ngrav, TEMPGRIDSIZE), 'temp0tab', zeros(if_ngrav, TEMPGRIDSIZE), ...
                      'flux', flux);
        
        fid=fopen(filename);
        
        for i=1:if_ngrav
            gin = textscan(fid, '%f %f', 1);
            output.gravity_list(i) = gin{2};
            temp_cell = textscan(fid, '%f %f %f', if_ntemp);
            tein = temp_cell{1};
            t0in = temp_cell{2};
            tintin = temp_cell{3};
            SZ = size(unique(tein),1);
            tein = tein(1:SZ);
            tintin = tintin(1:SZ);
            t0in = t0in(1:SZ);
            output.tempefftab(i,:) = interp1(tintin, tein, output.tempint_list, 'spline');
            output.temp0tab(i,:) = interp1(tintin, t0in, output.tempint_list, 'spline');
        end
        fclose(fid);
    end

    %% merge_boundaries - this procedure puts together multiple boundaries
    %% in the previous section.  Everything is placed over a standard grid,
    %% which allows for easier access in the future.
    function std_boundary = merge_boundaries(sub_bound_list)
        N = size(sub_bound_list,2);
        mingravity = min(sub_bound_list(1).gravity_list);
        maxgravity = max(sub_bound_list(1).gravity_list);
        for i=2:N
            mingravity = min([mingravity, min(sub_bound_list(i).gravity_list)]);
            maxgravity = max([maxgravity, max(sub_bound_list(i).gravity_list)]);
        end
        
        NumGravityPoints = 40;
        std_gravity_grid = log_grid(mingravity, maxgravity, NumGravityPoints);
        
        NumTempPoints = sub_bound_list(1).num_temp;
        tempint_list = sub_bound_list(1).tempint_list;
        minflux = sub_bound_list(1).flux;
        maxflux = sub_bound_list(N).flux;
        NumFluxPoints = 30;
        flux_list = log_grid(minflux, maxflux, NumFluxPoints);
        
        std_boundary = struct('gravity_list', std_gravity_grid, 'tempint_list', tempint_list, ...
                              'flux_list', flux_list, 'num_grav', NumGravityPoints, ...
                              'num_temp', NumTempPoints, 'num_flux', NumFluxPoints, ...
                              'pressure0', sub_bound_list(1).pressure0, ...
                              'temp_effective_table', zeros(NumFluxPoints, NumGravityPoints, NumTempPoints), ...
                              'temp0_table', zeros(NumFluxPoints, NumGravityPoints, NumTempPoints));
                          
        tmp_temp0_table = zeros(N, NumGravityPoints, NumTempPoints);
        tmp_temp_effective_table = zeros(N, NumGravityPoints, NumTempPoints);
                    
        input_flux_list = [];
        
        for i=1:N 
            input_flux_list = [input_flux_list, sub_bound_list(i).flux]; %#ok<AGROW>
            for j=1:NumTempPoints
                tmp_temp0_table(i,:,j) = interp1(log10(sub_bound_list(i).gravity_list), ...
                                                 sub_bound_list(i).temp0tab(:,j),...
                                                 log10(std_gravity_grid), 'spline');
                tmp_temp_effective_table(i,:,j) = interp1(log10(sub_bound_list(i).gravity_list), ...
                                                          sub_bound_list(i).tempefftab(:,j), ...
                                                           log10(std_gravity_grid), 'spline');                             
            end
        end
        
        for i=1:NumGravityPoints 
            for j=1:NumTempPoints
                std_boundary.temp_effective_table(:,i,j) = interp1(log10(input_flux_list), ...
                                                                   tmp_temp_effective_table(:,i,j), ...
                                                                   log10(flux_list), 'spline');
                std_boundary.temp0_table(:,i,j) = interp1(log10(input_flux_list), ...
                                                                   tmp_temp0_table(:,i,j), ...
                                                                   log10(flux_list), 'spline');
            end
        end
    end
    
    AU = 1.496e13;
    LumSun = 3.826e33;

    boundary_list = [load_sub_boundary('surfBC/grid9.5AU.dat', LumSun/(4.*pi*(AU*9.5)^2), 7, 16, 990), ...
                     load_sub_boundary('surfBC/1au_2.grid.XY', LumSun/(4.*pi*(AU*1.0)^2), 5, 13, 990), ...
                     load_sub_boundary('surfBC/0.1au.grid.fix.XY', LumSun/(4.*pi*(AU*0.1)^2),5,13,990), ...
                     load_sub_boundary('surfBC/grid045.gridH.XY', LumSun/(4.*pi*(AU*0.045)^2),5,12,990), ...
                     load_sub_boundary('surfBC/grid02.gridH.XY', LumSun/(4*pi*(AU*0.02)^2),5,12,990.)]; %#ok<NASGU>
                              
    std_bound = merge_boundaries(boundary_list); %#ok<NASGU>
    
    save('std_bound.mat', 'std_bound');
end